// alert('hi---1');

//$(document).on('udPageReady', function(){
//	(console) && console.log("udPageReady",arguments);
//});
$(document).on('bodyLoaded', function(){
    $(document).trigger('dc-after-load');
	var $links = $('#oppor_link1, #oppor_link2'),
		doc = $(document),
        fc = $('#firstcolumn'),
        sc = $('#secondcolumn'),
        hf = $('.heightfix'),
        mti = $('#myTabsId'),
        mtd = $('#myTabsDiv'),
        lb = $('#leftcol_bob'),
        $wd = $('#wrapper'),
		$ssc = $('#showsidecolumn'),
		$tab = $('.myTab'),
		myCliHTML = '',
		$acs = $('#allClientStates'),
		$mcs = $('#myClientStates'),
		$mc = $('#myClients'),
		$mb	= $('.myBk'),
		$ap = $('#alertPopup'),
		$rdo1 = $('#radio1'),
		$rdo2 = $('#radio2'),
		$curSels = $('.curSels'),
		$current = $('.current'),
		$css = $('#curSelsSec'),
		$ics = $('#instCurSel'),
		$sdb = $('#setDfltBook'),
		$sl = $('.setLbl'),
		$mcl = $('#mngCustLst'),
		$mcl1 = $('#mngCustLst1'),
		$rb = $('#renameBook'),
		$cb = $('#copyBook'),
		$curS = $('#currentSel'),
		$as = $('#actionSelect'),
		$nonS = $('#noneSelection'),
		$selected = $('.selected'),
		$ifWin = $('#infodivWin'),
		$in = $('#InstName'),
		$infoH = $('.infoHover'),
		$wProb = $('#winProbability'),
		$range1 = $('#range1'),
		$range2 = $('#range2'),
		$editLink = $('.editLink');
		$tab.tabs();
	/*setting Layout starts here*/
   
	/***************************Added for resizing *******************************/
	
	var wrap = $('#wrapper'),
		showcol = $('#showsidecolumn'),
		cont_scroll = $('.cont_scroll'),
		topH = $('#pagecontent').find('.pagetitle'),
		win = $(window);
	
	
	setTimeout(function(){
		var secCW = wrap.width()-fc.outerWidth()- 32;
		if($(window).width()<1280){
			sc.css('width','1000px').show();	
			cont_scroll.css('width','1200px').show();
		}else{
			sc.css('width',secCW+'px').show();
			cont_scroll.css('width','100%').show();
		}
		//cont_scroll.css('width',secCW+'px').show();
		if($('#toolBarCheck').length > 0){
			var winH = win.height() - 122;
		}else if($('.pagetitle').find('header').css('display') == 'none'){
			var winH = win.height() -100;
		}else{
			var winH = win.height() -140;
		}
		//wrap.css('height', winH+'px');
		win.resize(function(){
			var secCW = wrap.width()-fc.outerWidth()- 32;
			if($(window).width()<1280){
				sc.css('width','1000px').show();
				cont_scroll.css('width','1200px').show();	
			}else{
				sc.css('width',secCW+'px').show();
				cont_scroll.css('width','100%').show();
			}
			//sc.css('width',secCW+'px').show();
			if($('#toolBarCheck').length > 0){
				var winH = win.height() - 122;
			}else if($('.pagetitle').find('header').css('display') == 'none'){
				var winH = win.height() -100;
			}else{
				var winH = win.height() -140;
			}
			//wrap.css('height',winH+'px');
		});
	},20);
	
	
	
	
	$(document).on('click','#hidesidecolumn, #showsidecolumn', function(e){
			e.preventDefault();
			if($(this).attr('id') == 'hidesidecolumn'){
				fc.addClass('closed');
				showcol.parent().removeClass('closed');
				var secCW = wrap.width()-20;
				sc.css('width',secCW+'px');
			}else{
				fc.removeClass('closed');	
				showcol.parent().addClass('closed');
				var secCW = wrap.width()-fc.outerWidth()-30;
				sc.css('width',secCW+'px');
			}
		});
		
		/***************************End of Added for resizing *******************************/
	
	
	/* IE 6 & 7 popup open/close starts here */
    if ($('html').hasClass('ie7') == true || $('html').hasClass('ie6') == true) {
        $(document).on('click', 'a[href*="#"]', function(e) {
            var iePopups = $(this).attr('href');
            iePopups = '#' + iePopups.substring(iePopups.lastIndexOf('#') + 1);
            $(iePopups).dialog('open');
        });
    }
    /* IE 6 & 7 popup open/close ends here */
		
	/*Code for Selected Book starts here*/
	var $sel = $('.selBook');
	$(document).on('change', '#bookSelectList', function(e){
		if($(this).val() == 'ccb_1'){
			$sel.html('None');
		}else{
			$sel.html($(this).val());
		}		
	});
	/*Code for Selected Book ends here*/
	/*Code for My Clients and All Clients starts here*/
	$(document).on('click', '#radio1, #radio2', function(e) {
		if($(this).attr('id') == 'radio1'){
			$acs.addClass('closed');
			$mcs.removeClass('closed');
			$mc.append(myCliHTML);
			$mb.removeClass('closed');
		}else{
			$acs.removeClass('closed');
			$mcs.addClass('closed');
			myCliHTML = $mc.html();
			$mc.find('option').remove();
			$mb.addClass('closed');
		}
		if (($current.hasClass('closed') == false) || ($css.hasClass('closed') == false)) {
            $ap.dialog('open');
        }
    });
	/*Code for My Clients and All Clients ends here*/
	/*Code for Radio button Custome Book starts here*/
	$(document).on('click', '#cbradio1, #cbradio2, #cbradio3', function(e) {
        if ($(this).attr('id') == 'cbradio1') {
            $sdb.attr('disabled', true)
            $sl.addClass('dim');
        } else {
            $sdb.removeAttr('disabled');
            $sl.removeClass('dim');
        }
    });
	/*Code for Radio button Custome Book ends here*/
	/*Code for Radio button alert starts here*/
    $(document).on('click', '#cancelViewChange', function(e) {
        if ($rdo1.is(':checked') == true) {
            $rdo2.attr('checked', true);
        }
        if ($rdo2.is(':checked') == true) {
            $rdo1.attr('checked', true);
        }
    });	
    $(document).on('click', '#viewChange', function() {
		clearFunc();
        if ($rdo1.is(':checked') == true) {
            $rdo1.attr('checked', true);
        }
        if ($rdo2.is(':checked') == true) {
            $rdo2.attr('checked', true);           
        }
    });
	/*Code for Radio button alert ends here*/
    /*Code for Clear all alert ends here*/
	$(document).on('click', '#clearAllRef', function() {
		clearFunc();
    });
	/*Code for Clear all alert ends here*/
	function clearFunc(){
		$curS.find('ul').html('');
		$curSels.removeClass('closed');
        $nonS.removeClass('closed');
        $css.addClass('closed');
        $current.addClass('closed');
        $ics.html('None');
		lb.find('input[type=checkbox]').attr('checked', false);
	}
	/*Code for Take action starts here*/
    $(document).on('change', '#actionSelect', function() {
        var copyBk = $mcl.find('.shdhlt').text(),
            renBk = $mcl.find('.shdhlt').text();
        if ($(this).find(":selected").text() == "Rename" && $('#radio1').is(':checked') == true) {
            $rb.removeClass('closed');
            $rb.val(renBk);
        } else if ($(this).find(":selected").text() == "Rename" && $('#radio2').is(':checked') == true) {
            $rb.removeClass('closed');
        } else {
            $rb.addClass('closed');
        }
        if ($(this).find(":selected").text() == "Make Copy" && $('#radio1').is(':checked') == true) {
            $cb.removeClass('closed');
            $cb.val('Copy of' + ' ' + copyBk);
        } else if ($(this).find(":selected").text() == "Make Copy" && $('#radio2').is(':checked') == true) {
            $cb.removeClass('closed');
        } else {
            $cb.addClass('closed');
        }
    });
	/*Code for Take action ends here*/
	/*Code for Create book ends here*/
    $(document).on('click', '#createBook', function() {
		$('#createBookPopup').dialog('open');
		var $csp = $('#curSelPopup');
        $csp.html('');
        var creatbook = $curS.html();
        $csp.append(creatbook);
    });
	/*Code for Create book ends here*/
	/* Delete functionality for Current selection starts here*/
    $(document).on('click', '.icdeleteIcon', function() {
		var $li = $(this).closest('li'),
			$liC = $li.attr('class');
			$('li[data-class='+$liC+']').find('input').attr('checked', false);
		if($curS.find('li').length <= 2){
			$curS.closest('.scrollContainer').addClass('closed');
			$nonS.removeClass('closed');
		}else{
			$curS.closest('.scrollContainer').removeClass('closed');
			$nonS.addClass('closed');
		}
        $li.remove();
    });
	/* Delete functionality for Current selection ends here*/
	/*Code for Book selection on popup ends here*/
	$(document).on('click', '#mngCustLst .flevel li', function(e) {
        $(this).addClass('shdhlt').siblings().removeClass('shdhlt').closest('.flevel').siblings().find('li').removeClass('shdhlt');
        $as.removeAttr('disabled');
        if ($(this).hasClass('gblbk') == true) {
            $as.find('option:eq(2)').attr('disabled', true);
        } else {
            $as.find('option:eq(2)').removeAttr('disabled');
        }
        if ($(this).hasClass('gbldra') == true) {
            $as.find('option:eq(3)').removeAttr('disabled');
        } else {
            $as.find('option:eq(3)').attr('disabled', true);
        }
    });
	/*Code for Book selection on popup ends here*/
	/*Code for panel accordion starts here
    $('.hd').find('h3').on('click', function(e) {
        e.preventDefault();
		$infoH.addClass('hidden').removeClass('visible');
        if ($(this).closest('div').hasClass('hdexpanded') == true) {
            $(this).find('a').addClass('collapsed').removeClass('expanded');
            $(this).closest('div').next().addClass('expanded bgexpanded');
        } else {
            $(this).closest('div').next().addClass('expanded bgexpanded').siblings('.content').removeClass('expanded bgexpanded').hide();
            $(this).closest('div').next().siblings('.hd').removeClass('hdexpanded').find('a').addClass('collapsed').removeClass('expanded');
        }
    });
	Code for panel accordion ends here*/
	/*Code for secondlevel starts here*/
    $('#leftcol_bob .secondlevel').on('click', function(e) {
        if ($(this).attr('checked')) {
            $(this).closest('ul').parent().addClass('activeli');
            $(this).closest('content').parent().addClass('activeli');
        } else {
            $(this).closest('ul').parent().removeClass('activeli');
        }
    });
	/*Code for secondlevel ends here*/
	/*Select all functionality for My Team starts here*/
    $(document).on('click', '#selectAll', function() {
		var selAll = $('#selectAllOpen');
        if($(this).is(':checked')){
            selAll.find('input[type=checkbox]').attr('checked', true);
            selAll.find('input[type=checkbox]').trigger('click');
            selAll.find('.content1').addClass('closed');
            selAll.find('.clearspan').addClass('closed');
            selAll.find('.editLink').removeClass('closed');
            selAll.find('.firstlevel').attr('disabled', true);
			$curS.closest('.scrollContainer').removeClass('closed');
        }else {
            selAll.find('input[type=checkbox]').attr('checked', false);
			var rel = $(this).closest('.content').attr('rel');
			$curS.find('#rel'+rel).html('');
			if($curS.find('li').length < 2){
				$curS.closest('.scrollContainer').addClass('closed');
				setTimeout(function(){
					$nonS.removeClass('closed');
				},500);
			}
        }
    });
	/*Select all functionality for My Team starts here*/
	/*First Level Functionality starts here*/
    $('#leftcol_bob a.editLink').on('click', function(e) {
        $(this).closest('li').find('.secondleveldiv').addClass('closed');
        $(this).closest('li').find('.secondleveldiv').removeClass('closed');
        $(this).closest('li').siblings().addClass('shd3').find('.content1').addClass('closed');
        $(this).closest('.content').find('li.shd3').each(function(index) {
            $(this).removeClass('shd3');
            $(this).find('.clearspan').addClass('closed');
            $(this).find('.editLink').removeClass('closed');
            $(this).find('.secondleveldiv li').each(function(index) {
                if ($(this).hasClass('activeli') && $(this).parent().hasClass('nlist')) {
                    $(this).removeClass('closed');
                } else {
                    if ($(this).parent().hasClass('nlist')) {
                        $(this).addClass('closed');
                    }
                }
            });
        });
        $(this).closest('li').find('.clearspan').removeClass('closed');
        $(this).closest('li').addClass('shd3');
        $(this).closest('li').find('.secondleveldiv').removeClass('closed');
        $(this).closest('li').find('.secondleveldiv').find('.secondlevel').removeClass('hidden');
        $(this).addClass('closed');
        $(this).closest('li').find('.secondleveldiv li').each(function(index) {
            $(this).parent().parent().removeClass('closed');
            if ($(this).parent().hasClass('nlist')) {
                $(this).find('input[type=checkbox]').removeClass('closed');
                $(this).removeClass('closed');
            }
        });
        $(this).closest('li').find('.firstleveldiv').find('input[type=checkbox]').prop('disabled', false);
        $(this).closest('li').find('.secondleveldiv').find('.teamScroll').removeClass('closed');
    });
	/*First Level Functionality ends here*/
	/*First Level close Functionality starts here*/
    $('#leftcol_bob .icclearLink').on('click', function(e) {
        $(this).parent().parent().removeClass('shd3');
        $(this).parent().parent().find('.editLink').removeClass('closed');
        $(this).parent().addClass('closed');

        $(this).parent().parent().find('.secondleveldiv').addClass('closed');
        var ct = 0;
        $(this).parent().parent().find('.secondleveldiv li').each(function() {
            var chkbox = $(this).find('input[type=checkbox]').is(':checked'),
                radBtn = $(this).find('input[type=radio]').is(':checked');
            if (chkbox == true || radBtn == true) {
                ct++;
            }
        });
        $(this).closest('li').find('.firstleveldiv').find('input[type=checkbox]').prop('disabled', true);
        $(this).closest('li').find('.secondleveldiv').find('.teamScroll').addClass('closed');
    });
	/*First Level close Functionality ends here*/
	/*Parent Checkbox Selects All starts here*/
    $(document).on('change', '.firstlevel', function(e) {
        if ($(this).is(':checked')) {
			$(this).closest('li').find('.secondleveldiv').find('input[type=checkbox]').trigger('click');
			setTimeout(function(){ 
				$(this).closest('li').find('.secondleveldiv').find('input[type=checkbox]').attr('checked', true);
			}, 500);
            $('.secondlevel').closest('ul').parent().addClass('activeli');
        } else {
            $(this).closest('li').find('.secondleveldiv').find('input[type=checkbox]').prop('checked', false);
            $('.secondlevel').closest('ul').parent().removeClass('activeli');
        }
        $(window).resize();
    });
	/*Parent Checkbox Selects All ends here*/
	/*secondleveldiv checkbox functionality starts here*/
    var ct = 0;
    $('.secondleveldiv').find('input[type=checkbox]').on('click', function() {
        if ($(this).is(":checked") == false) {
            $(this).closest('.secondleveldiv').parent('li').find('.firstlevel').prop('indeterminate', true);
        }
    });
	/*secondleveldiv checkbox functionality starts here*/
	/*Interaction last visit starts here*/
	$('.rad').on('click', function(e) {
        $('.sel').remove();
		if ($(this).is(":checked") == true) {
			$(this).closest('ul').parent().addClass('activeli').siblings().addClass('activeli');
			var radio = $(this).next().text(),
				curr = $(this).closest('.content').prev().text();
				$curS.append('<li class="sel"><span class="clsicn">' + curr + ':<span class="mls"></span>' + radio + '</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
				$nonS.addClass('closed');
				$current.removeClass('closed');
		}else{
			$(this).closest('ul').parent().addClass('activeli').siblings().removeClass('activeli');
			$current.addClass('closed');
			$nonS.removeClass('closed');
		}        
            
    });
	/*Interaction last visit ends here*/
    $("#timeframe-slider").slider({
        orientation: "horizontal",
        range: true,
        value: 100,
        min: 0,
        max: 100,
        step: 1,
        values: [10, 30],
        slide: function(event, ui) {
            $("#range1").val(ui.values[0]);
            $("#range2").val(ui.values[1]);
        }
    });

    $(document).on('keyup', '#range1, #range2', function() {
        var $ran1 = $(this).val(),
            num = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/,
            slider = $(this).closest('.infoHover').find('.slider');
        if (!num.test($ran1) || $ran1 > 100) {
            $(this).addClass('descl');
        } else {
            $(this).removeClass('descl');
            if ($(this).attr('id') == 'range1') {
                slider.find('a').eq(0).css({'left': $ran1 + '%'});
            } else {
                slider.find('a').eq(1).css({'left': $ran1 + '%'});
            }
        }
        var r1 = $range1.val(),
            r2 = $range2.val(),
			rng = r1 - r2;
        if (r1 > r2) {
            $(this).closest('.infoHover').find('.ui-slider-range').css({'left': r2 + '%','width': rng + '%'});
        } else {
            $(this).closest('.infoHover').find('.ui-slider-range').css({'left': r1 + '%','width': rng + '%'});
        }
    });
	
	$('#applySlider').on("click", function(e) {
		$('.wProb').remove();
        $('#range1Val').html($range1.val());
        $('#range2Val').html($range2.val());
        $wProb.attr('checked', true);
        $ifWin.addClass('hidden').removeClass('visible');
        var title = $('.infoPopup').closest('li').text();
        $curS.append('<li class="wProb"><span>' + title + '</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span>');
        $current.removeClass('closed');
        $wProb.prop('disabled', true);
        $wProb.closest('li').removeClass('shd3');
    });

    $('#leftcol_bob .firstleveldiv').on('click', function(e) {
        if ($(this).attr('id') == 'winProbability') {
            e.preventDefault();
        } else {
            $(this).closest('li').find('a.editLink').trigger('click');
        }
    });

    mtd.on('scroll', function() {
        $infoH.addClass('hidden').removeClass('visible');
    });

    $('.selview').on('click', function() {
        $('.tbldata').addClass('closed');
        $('li ul, li div.scrollContainer').addClass('closed');
        $('div.first h3 a').html('<span class="icon"></span>' + $(this).parent('li').find('span.txtlc').text() + ' Summary');
        if ($(this).attr('id') == 'Bob' || $(this).attr('id') == 'Market' || $(this).attr('id') == 'allClients') {
            $('#ibob').removeClass('closed');
        } else if ($(this).attr('id') == 'Region') {
            $('#regions').removeClass('closed');
        } else {
            $('#stat').removeClass('closed');
        }
        var subID = 'sub' + $(this).attr('id');
        if ($('#' + subID).length) {
            $('#' + subID + ',#' + subID + ' ul').removeClass('closed');
        }
    });

    $('.narview').on('click', function() {
        $('.subnarrow').addClass('closed');
        var subnarview = 'sub' + $(this).attr('id');
        $('.' + subnarview).removeClass('closed');
    });
	/* More/Fewer functionality starts here*/
    // $(document).on('click', '#moreFewV', function(e) {
    //     e.preventDefault();
    //     if ($(this).html() == 'More..') {
    //         $('.moreV').removeClass('hidden');
    //         $(this).html('Fewer..').attr('title', 'Fewer');
    //     } else {
    //         $('.moreV').addClass('hidden');
    //         $(this).html('More..').attr('title', 'More');
    //     }
    // });
	/* More/Fewer functionality ends here*/
	/* Current Selection None functionality starts here*/
	$(document).on('click', 'input[type=checkbox]', function(e) {
		$infoH.addClass('hidden').removeClass('visible');
		if (!$curS.text() == '') {
            $nonS.addClass('closed');
        } else {
            $nonS.removeClass('closed');
        }
    });
	/* Current Selection None functionality ends here*/
	/* Callout functionality starts here*/
	function showInfoHover(obj, objdiv, pos) {
		$infoH.addClass('hidden').removeClass('visible');
		var thisPos = $(obj).closest('li').offset();
		var leftPos = thisPos.left + $(obj).closest('li').width() + 25;  // px for padding, border
		var infoHoverHeight = $(objdiv).height();
		var topPos = thisPos.top-($(objdiv).height()/2); // px for dropdown icon width
		var wH = $(window).height()
		var dynPos = $(obj).offset().top+($(objdiv).height()/2);
		var pntrOT=$(objdiv).height()/2;  
		var gtW = (dynPos-wH)-15;
		if(dynPos > wH){
			var pntrOT = (150/2)+gtW;
			$(objdiv).css('top', (topPos-gtW) + 'px').css('left', (leftPos) + 'px').addClass('visible').removeClass('hidden');
			$(objdiv).find('.pointer').css('top',(pntrOT+10)+'px');
		}else{
			$(objdiv).css('top', (topPos) + 'px').css('left', (leftPos) + 'px').addClass('visible').removeClass('hidden');
			$(objdiv).find('.pointer').css('top',(pntrOT+15)+'px');
		}
	}
	$(document).on('click', '#winCancel, #assetCancel', function(e) {
       $infoH.addClass('hidden').removeClass('visible');
    });
	$(document.body).on('click',function(e) {
		if(!$(e.target).closest('#winProb').length>0){
			$infoH.addClass('hidden').removeClass('visible');
		}
    });
	$(document).on('click', '#winProb', function(e) {
        e.preventDefault();
        $(this).closest('li').addClass('shd3');
        $(this).closest('li').addClass('selected').siblings().removeClass('selected');
        $editLink.removeClass('closed');
		showInfoHover(this, '#infodivWin');
    });
    $(document).on("click", "#oppor_link2", function(e) {
        if ($(this).hasClass('disabled') == true) {
            e.preventDefault();
            $(this).closest('li').removeClass('shd3').find('.disabled').removeClass('closed');
        } else {
            $('#investmentId').addClass('visible').removeClass('hidden');
            showInfoHover(this, '#investmentId');
            $editLink.removeClass('closed');
            $('#oppor_link2').closest('li').removeClass('shd3');
            $('#assetClassId').addClass('hidden');
            $('#transferVendor').addClass('hidden');
            return false;
        }
    });

    $(document).on("click", "#oppor_link1", function(e) {
        if ($(this).hasClass('disabled') == true) {
            e.preventDefault();
            $(this).closest('li').removeClass('shd3').find('.disabled').removeClass('closed');
        } else {
            $('#assetClassId').addClass('visible').removeClass('hidden');
            showInfoHover(this, '#assetClassId');
            $editLink.removeClass('closed');
            $('#oppor_link1').closest('li').removeClass('shd3');
            $('#investmentId').addClass('hidden');
            $('#transferVendor').addClass('hidden');
            return false;
        }
    });
	/* Callout functionality ends here*/
    $('.btncancel').on('click', function(e) {
        $('.infoPopup').closest('li').removeClass('shd3');
        $('.infoHover').addClass('closed').removeClass('visible');
        $('#assetClass1').prop('disabled', true);
        $('#date').prop('disabled', true);
    });

    var cntnl = 0;
   $('.checked').on('click', function(e) {
		var lnk = $(this).next().text().replace(/[^A-Z0-9]+/ig, "_");
		if ($(this).is(':checked')){
            $css.find('.selNlist').each(function() {
                cntnl++;
            });
            var rel = $(this).closest('.content').attr('rel');
			$(this).closest('li').attr('data-class',lnk);
			if (cntnl > 0) {
                var curr = $(this).closest('.content').prev().text();
                $('.selNlist').find('#rel1' + rel).append('<li class="'+lnk+'"><span class="clsicn">' + curr + ':<span class="mls"></span>' + $(this).next().text() + '</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
            } else {
                var curr = $(this).closest('.content').prev().text();
                $curS.find('#rel' + rel).append('<li class="'+lnk+'"><span class="clsicn">' + curr + ':<span class="mls"></span>' + $(this).next().text() + '</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
                $current.removeClass('closed');
            }
        }else{
			$('.'+lnk).remove();
			if($curS.find('li').length <= 1){
				$current.addClass('closed');
				$nonS.removeClass('closed');
			}else{
				$current.removeClass('closed');
				$nonS.addClass('closed');
			}
		}
    });

    var ct = 0;
    $('.btnapply').on('click', function(e) {
        $selected.find('input[type=checkbox]').attr('checked', false);
        $selected.find('.append').html('');
        var current = ($selected.find('input[type=checkbox]').next().text());
        $(this).closest('.infoHover').find('input:checked').each(function(e, obj) {
            ct++;
            $ics.html(ct + ' Institutions meet current selections:');
            $selected.find('.append').append($(obj).next().text() + '<br />');
            $curS.append('<li><span>' + current + ':' + $(obj).next().text() + '</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
        });
        $selected.find('.append').removeClass('closed');
        $('.infoHover').addClass('closed').removeClass('visible');
        $('.infoPopup').closest('li').removeClass('shd3');
        $selected.find('input[type=checkbox]').attr('checked', true);
        $current.removeClass('closed');
    });
    $(document).on('click', '.allSel', function() {
        $(this).closest('ul').find('.sel').attr('checked', this.checked);
    });
    $(document).on('click', '.sel', function() {
        if ($(this).closest('div').find('.sel').length == $(this).closest('div').find('.sel:checked').length) {
            $(this).closest('div').find('.allSel').attr("checked", "checked");
        } else {
            $(this).closest('div').find('.allSel').removeAttr("checked");
        }
    });
	$('#assetapply').on("click", function(e) {
        var curr = $('#assetClass1').next().text();
        var rel = $('#oppor_link1').closest('.content').attr('rel');
        if ($('.allSel:checked').length > 0) {
            $curS.find('#rel' + rel).append('<li><span class="clsicn">' + curr + ':<span class="mls"></span>' + $(".allSel").next().text() + '</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
            $current.removeClass('closed');
        } else {
            $('.sel:checked').each(function() {
                $curS.find('#rel' + rel).append('<li><span class="clsicn">' + curr + ':<span class="mls"></span>' + $(this).next().text() + '</span><span class="mls"></span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
                $current.removeClass('closed');
            });
        }
        if ($('input[name=ac]:checked').length > 0) {
            var asof = $('#testinput-Date-picker1').val();
            $curS.find('#rel' + rel).append('<li><span class="clsicn">' + curr + ':<span class="mls"></span>' + $("input[name=ac]:checked").next().text() + ' ' + asof + '</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
            $current.removeClass('closed');
        }
        $('#assetClass1').prop('checked', true);
        $('#assetClass1').prop('disabled', true);
        $('.infoHover').addClass('closed').removeClass('visible');
    });

    $('#btnapply').on("click", function(e) {
        var curr = $('#investments').next().text();
        var rel = $('#oppor_link2').closest('.content').attr('rel');
        if ($('.fcompany:checked').length > 0) {
            $curS.find('#rel' + rel).append('<li><span class="clsicn">' + curr + ':<span class="mls"></span>' + $(".fcompany").next().text() + '</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
            $current.removeClass('closed');
        } else if ($('.iType:checked').length > 0) {
            $curS.find('#rel' + rel).append('<li><span class="clsicn">' + curr + ':<span class="mls"></span>' + $(".iType").next().text() + '</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
            $current.removeClass('closed');
        } else {
            $('.sels:checked').each(function() {
                $curS.find('#rel' + rel).append('<li><span class="clsicn">' + curr + ':<span class="mls"></span>' + $(this).next().text() + '</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
                $current.removeClass('closed');
            });
        }
        $('#investments').prop('checked', true);
        $('#investments').prop('disabled', true);
        $('#investments').closest('li').removeClass('shd3');
    });

    $(document).on("change", '#year, #yearDis', function(e) {
        var curDate = new Date(),
            curM = curDate.getMonth() + 1,
            curY = curDate.getFullYear(),
            preY = curY - 1;
        if ($(this).attr('id') == 'year') {
            if ($(this).val() == preY) {
                $('#month').val(curM);
                $('#month').find('option').removeAttr('disabled');
                $('#month').find('option').each(function() {
                    if ($(this).val() < curM) {
                        $(this).attr('disabled', true);
                    }
                });
            } else {
                $('#month').val(curM);
                $('#month').find('option').removeAttr('disabled');
                $('#month').find('option').each(function() {
                    if ($(this).val() > curM) {
                        $(this).attr('disabled', true);
                    }
                });
            }
        } else {
            if ($(this).val() == preY) {
                $('#monthDis').val(curM);
                $('#monthDis').find('option').removeAttr('disabled');
                $('#monthDis').find('option').each(function() {
                    if ($(this).val() < curM) {
                        $(this).attr('disabled', true);
                    }
                });
            } else {
                $('#monthDis').val(curM);
                $('#monthDis').find('option').removeAttr('disabled');
                $('#monthDis').find('option').each(function() {
                    if ($(this).val() > curM) {
                        $(this).attr('disabled', true);
                    }
                });
            }
        }
    });

    $(document).on("change", '#year1, #year1Dis', function(e) {
        var curDate = new Date(),
            curM = curDate.getMonth() + 1,
            curY = curDate.getFullYear(),
            preY = curY - 1;
        if ($(this).attr('id') == 'year1') {
            if ($(this).val() == preY) {
                $('#month1').val(curM);
                $('#month1').find('option').removeAttr('disabled');
                $('#month1').find('option').each(function() {
                    if ($(this).val() < curM) {
                        $(this).attr('disabled', true);
                    }
                });
            } else {
                $('#month1').val(curM);
                $('#month1').find('option').removeAttr('disabled');
                $('#month1').find('option').each(function() {
                    if ($(this).val() > curM) {
                        $(this).attr('disabled', true);
                    }
                });
            }
        } else {
            if ($(this).val() == preY) {
                $('#month1Dis').val(curM);
                $('#month1Dis').find('option').removeAttr('disabled');
                $('#month1Dis').find('option').each(function() {
                    if ($(this).val() < curM) {
                        $(this).attr('disabled', true);
                    }
                });
            } else {
                $('#month1Dis').val(curM);
                $('#month1Dis').find('option').removeAttr('disabled');
                $('#month1Dis').find('option').each(function() {
                    if ($(this).val() > curM) {
                        $(this).attr('disabled', true);
                    }
                });
            }
        }
    });

    $(document).on('click', '#fundComp', function(e) {
        $('#FundCompany').removeClass('closed');
        $('#InvestType').addClass('closed');
    });

    $(document).on('click', '#investType', function(e) {
        $('#InvestType').removeClass('closed');
        $('#FundCompany').addClass('closed');
    });

    $('.thirdlev').on('change', function() {
        if ($(this).closest('ul').find('input[type=checkbox]').length == $(this).closest('ul').find('input:checked').length) {
            $(this).closest('.content').prev().find('.firstlev').attr('checked', true)
        } else {
            $(this).closest('.content').prev().find('.firstlev').attr('checked', false);
        }
    });

    $('.sublev').on('change', function() {
        if ($(this).closest('.content').find('input[type=checkbox]').length == $(this).closest('.content').find('input:checked').length) {
            $(this).closest('.content').prev().find('.firstlev').attr('checked', true)
        } else {
            $(this).closest('.content').prev().find('.firstlev').attr('checked', false);
        }
    });

    $('.sublev1').on('change', function() {
        if ($(this).closest('.content').find('input[type=checkbox]').length == $(this).closest('.content').find('input:checked').length) {
            $(this).closest('.content').prev().find('.firstlev').attr('checked', true)
        } else {
            $(this).closest('.content').prev().find('.firstlev').attr('checked', false);
        }
    });

    $(document).on('click', '.allSels', function() {
        $(this).closest('.scrollCont1').find('input[type=checkbox]').attr('checked', this.checked);
    });

    $(document).on('click', '.sels', function() {
        if ($(this).closest('.scrollCont1').find('.sels').length == $(this).closest('.scrollCont1').find('.sels:checked').length) {
            $(this).closest('.scrollCont1').find('.allSels').attr("checked", "checked");
        } else {
            $(this).closest('.scrollCont1').find('.allSels').removeAttr("checked");
        }
    });

    $('#investmentId .planpanel .hd input[type=checkbox]').on('click', function(e) {
        e.stopImmediatePropagation();
        if ($(this).attr('checked')) {
            $(this).closest('.hd').next().find('input[type=checkbox]').attr("checked", "checked");
        } else {
            $(this).closest('.hd').next().find('input[type=checkbox]').removeAttr("checked");
        }
    });

    $('#byregion .planpanel .hd input[type=checkbox]').on('click', function(e) {
        e.stopImmediatePropagation();
        if ($(this).attr('checked')) {
            $(this).closest('.hd').next().find('input[type=checkbox]').attr("checked", "checked");
        } else {
            $(this).closest('.hd').next().find('input[type=checkbox]').removeAttr("checked");
        }
    });

    $(document).on('click', '.closeLink', function(e) {
        e.preventDefault();
        $(this).closest('.alertInsti').removeClass('visible');
        $('.instPag').removeClass('closed');
    });

    $(document).on('click', '#searchInstitute-btn1', function(e) {
        $('.imageLoad').removeClass('closed');
        $('.alertInsti').removeClass('visible');
        setTimeout(function() {
            var len = $('#searchInstitute').val().length;
            leng = $('#searchInstitute').val(),
            instname = $('#InstName'),
            Iname = $('#searchInstitute').val();
            se = 0;
            if (len >= 3 && leng == "har") {
                $('.alertInsti').addClass('visible');
                $('.imageLoad').addClass('closed');
                $('#instiResults').removeClass('hidden');
                $(instname).removeClass('closed');
                pageSize = 10;
                showPage = function(page) {
                    $(".check").hide();
                    $(".check").each(function(n) {
                        if (n >= pageSize * (page - 1) && n < pageSize * page)
                            $(this).show();
                    });
                }
                showPage(1);
                $("#Instipagi a").click(function() {
                    $('.imageLoad1').removeClass('closed');
                    var relVal = $(this).attr('rel');
                    setTimeout(function() {
                        $("#Instipagi a").removeClass("current");
                        $(this).addClass("current");
                        if (relVal == '1') {
                            $('.pageNum').html('1-10');
                        } else {
                            $('.pageNum').html('11-20');
                        }
                        $('.imageLoad1').addClass('closed');
                    }, 1000);
                    showPage(parseInt($(this).attr('rel')))
                });
                $(instname).find('input[type=checkbox]').attr('checked', false);
                $(instname).find('li').removeClass('matched').addClass('closed');
                $(instname).find('li').each(function() {
                    var ListName = $(this).text(),
                        ser = ListName.toLowerCase().indexOf(Iname);
                    if (ser >= 0) {
                        se++;
                        $(this).addClass('matched');
                        $('.matched').closest('li').removeClass('closed');
                    } else {
                        $(this).removeClass('matched');
                    }
                });
            } else if (len >= 3) {
                $('.imageLoad').addClass('closed');
                $('#instiResults').addClass('hidden');
                $(instname).removeClass('closed');
                $('#Instipagi').addClass('closed');
                $(instname).find('input[type=checkbox]').attr('checked', false);
                $(instname).find('li').removeClass('matched').addClass('closed');
                $(instname).find('li').each(function() {
                    var ListName = $(this).text();
                    ser = ListName.toLowerCase().indexOf(Iname);
                    if (ser >= 0) {
                        se++;
                        $(this).addClass('matched');
                        $('.matched').closest('li').removeClass('closed');
                    } else {
                        $(this).removeClass('matched');
                    }
                });
            } else {
                e.preventDefault();
            }
        }, 3000);
    });

    $(document).on('click', '#applyInsti', function(e) {
        var curr = $(this).closest('.content').prev().text();
        var rel = $(this).closest('.content').attr('rel');
        $in.find('input:checked').each(function(e) {
            $curS.find('#rel' + rel).append('<li><span class="clsicn">' + curr + ':<span class="mls"></span>' + $(this).next().text() + '</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
            $current.removeClass('closed');
        });
        $in.addClass('closed');
    });

    $(document).on('click', '#cancelInsti', function(e) {
        $in.find('input:checked').each(function(e) {
            $(this).attr('checked', false);
        });
    });

    $(document).on('click', '#applyLocation', function(e) {
        var rel = $(this).closest('.content').attr('rel');
        if ($('#state').is(':checked') == true) {
            var curr = $('#state').val();
            if ($('.states:checked').length > 0) {
                $curS.find('#rel' + rel).append('<li><span class="clsicn">' + curr + ':<span class="mls"></span>' + $(".states").next().text() + '</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
                $current.removeClass('closed');
            } else {
                $('.sels:checked').each(function() {
                    $curS.find('#rel' + rel).append('<li><span class="clsicn">' + curr + ':<span class="mls"></span>' + $(this).next().text() + '</span><span class="mls"></span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
                    $current.removeClass('closed');
                });
            }
        } else {
            $('#byregion').find('input:checked').each(function(e) {
                var curr = $(this).closest('.content').prev().find('input[type=checkbox]').next().text();
                $curS.find('#rel' + rel).append('<li><span class="clsicn">' + curr + ':<span class="mls"></span>' + $(this).next().text() + '</span><span class="icdeleteIcon icon flr mts custIcon" title="Close">Delete</span></li>');
                $current.removeClass('closed');
            });
        }
    });

    $(document).on('click', '#cancelLocation', function(e) {
        if ($('#state').is(':checked') == true) {
            $('#bystate').find('input:checked').each(function(e) {
                $(this).attr('checked', false);
            });
        } else {
            $('#byregion').find('input:checked').each(function(e) {
                $(this).attr('checked', false);
            });
        }
    });
	
	$(document).on('click', '#manageBook', function(e) {
		e.preventDefault();
		$('#manageBookPopup').dialog('open');
		$('#alertModule').addClass('hidden').removeClass('visible');
		$('#actionSelect').val('sel').attr('disabled', true);
		$('#renameBook').addClass('closed');
		$('#mngCustLst').find('li').removeClass('shdhlt');
    });
	
	$tab.find('li').on('click', function(e) {
		if ($(this).index() == 6) {
		    $links.addClass('disabled').closest('li').find('label').addClass('dim');
        } else {
            $links.removeClass('disabled').closest('li').find('label').removeClass('dim');
        }
    });
	
	var userInfo = store.get('user');
	
	$(document).on('click', '#state', function() {
		$('#bystate').removeClass('closed');
		$('#byregion').addClass('closed');
		if (userInfo == "UWC") {
			$('#myClientStates').addClass('closed');
			$('#allClientStates').removeClass('closed');
		}else{
			$('#myClientStates').removeClass('closed');
			$('#allClientStates').addClass('closed');
		}
	});
	
	$(document).on('click', '#region', function() {
		$('#bystate').addClass('closed');
		$('#myClientStates').addClass('closed');
		$('#byregion').removeClass('closed');
		$('#allClientStates').addClass('closed');
	});
	
	$(document).on('click', '#delBook', function() {
		var delItem = $('#mngCustLst').find('.shdhlt').attr('rel');
		$('#mngCustLst').find('.shdhlt').remove();
		$('#bookSelectList').find('option[rel=' + delItem + ']').remove();
		$('#deleteBook').dialog('close');
		$('#manageBookPopup').dialog('close');
		$('.selBook').html('None');
	});
	
	$(document).on('click', '#applyMngCust', function(e) {
		var actSel = $('#actionSelect').find(":selected").text(),
			renBk = $('#renameBook').val();
		if(actSel == "Rename" && $('#radio1').is(':checked') == true){
			if(renBk != ''){
				var renItem = $('#mngCustLst').find('.shdhlt').attr('rel');
				$('#mngCustLst').find('.shdhlt').html('<span>' + renBk + '</span>');
				$('#myClients').find('option[rel=' + renItem + ']').text(renBk).attr('selected', true);
				$('.selBook').html(renBk);
				$('#manageBookPopup').dialog('close');
			}else{
				$('#alertModule').addClass('visible').removeClass('hidden');
			}
		}else if(actSel == "Rename" && $('#radio2').is(':checked') == true){
			if(renBk != ''){
				var renItem = $('#mngCustLst').find('.shdhlt').attr('rel');
				$('#mngCustLst').find('.shdhlt').html('<span>' + renBk + '</span>');
				$('#allClients').find('option[rel=' + renItem + ']').text(renBk).attr('selected', true);
				$('.selBook').html(renBk);
				$('#manageBookPopup').dialog('close');
			}else{
				$('#alertModule').addClass('visible').removeClass('hidden');
			}
		}else if(actSel == "Set As Default Book"){
			var mcl = $('#mngCustLst1').find('.shdhlt').text();
			if($('#radio1').is(':checked') == true){
				$('#myClients').append('<option selected="selected">' + mcl + '</option>');
			}else{
				$('#allClients').append('<option selected="selected">' + mcl + '</option>');
			}
			$('#manageBookPopup').dialog('close');
		}else if(actSel == "Delete"){
			$('#bnam').html('');
			var delItem = $('#mngCustLst').find('.shdhlt').html();
			$('#bnam').append(' ' + delItem);
			$('#bnam').removeClass('closed');
			$('#deleteBook').dialog('open');
			$('#manageBookPopup').dialog('close');
		}else if(actSel == "Publish Draft"){
			$('#manageBookPopup').dialog('close');
			$('#mngCustLst').find('.shdhlt').removeClass('gbldra');
			var myString = $('#mngCustLst').find('.shdhlt').html(),
				draftItem = $('#mngCustLst').find('.shdhlt').attr('rel');
			$('#mngCustLst').find('.shdhlt').html(myString.substr(0, myString.length-7));
			$('#globalClients').find('option[rel=' + draftItem + ']').text(myString.substr(0, myString.length-7));
		}
	});
	
	lb.find('.hd h3').off('click').on('click', function(e){
		if($(this).text() == 'Segment'){
			$('#ui-id-1').trigger('click');
		}else if($(this).text() == 'Interactions'){
			$('#ui-id-2').trigger('click');
		}else if($(this).text() == 'Opportunities'){
			$('#ui-id-3').trigger('click');
		}else if($(this).text() == 'Risk'){
			$('#ui-id-4').trigger('click');
		}else if($(this).text() == 'Assets & Contributions'){
			$('#ui-id-5').trigger('click');
		}else if($(this).text() == 'Transfers'){
			$('#ui-id-7').trigger('click');
		}else if($(this).text() == 'Distribution'){
			$('#ui-id-8').trigger('click');
		}else{
			
		}
		$infoH.addClass('hidden').removeClass('visible');
        if ($(this).closest('div').hasClass('hdexpanded') == true) {
            $(this).find('a').addClass('collapsed').removeClass('expanded');
            $(this).closest('div').next().addClass('expanded bgexpanded');
        } else {
            $(this).closest('div').next().addClass('expanded bgexpanded').siblings('.content').removeClass('expanded bgexpanded').hide();
            $(this).closest('div').next().siblings('.hd').removeClass('hdexpanded').find('a').addClass('collapsed').removeClass('expanded');
        }
	});
	
	if (userInfo == "UWC") {
        $('#clientSelector').removeClass('closed');
        $('#bookselect2').addClass('closed');
        $('#bystate').addClass('closed');
        $('#byregion').removeClass('closed');       
        $(document).on('click', '#saveCustomBook', function(e) {
            e.preventDefault();
            var $bnp = $('#bookNamePopup'),
				bnp = $('#bookNamePopup').val(),
                bnpL = bnp.length,
                lstC = bnp.substr(0, 3),
                sdb = $('#setDfltBook').is(':checked'),
                chkdRdo = $('#bookNameRadio').find('input[type=radio]:checked').attr('rel'),
                chkd = chkdRdo.split(' ');
            if ($('#radio1').is(':checked') == true) {
                if (sdb == true) {
                    $(chkd[1]).append('<option class="txtblack" value="' + bnp + '" selected="selected" rel="' + lstC + '">' + bnp + '</option>');
                    $(chkd[0]).append('<li rel="' + lstC + '">' + bnp + '</li>');
                } else {
                    $(chkd[1]).append('<option class="txtblack" value="' + bnp + '" rel="' + lstC + '">' + bnp + '</option>');
                    $(chkd[0]).append('<li class="myBk" rel="' + lstC + '">' + bnp + '</li>');
                }
            } else if ($('#radio2').is(':checked') == true) {
                if (sdb == true) {
                    $(chkd[1]).append('<option class="txtblack" value="' + bnp + '" selected="selected" rel="' + lstC + '">' + bnp + '</option>');
                    $(chkd[0]).append('<li rel="' + lstC + '">' + bnp + '</li>');
                } else {
                   $(chkd[1]).append('<option class="txtblack" value="' + bnp + '" rel="' + lstC + '">' + bnp + '</option>');
                    $(chkd[0]).append('<li rel="' + lstC + '">' + bnp + '</li>');
                }
            } else {

            }
            if ($bnp.val() == '') {
                e.preventDefault();
                $('#alertModule1').addClass('visible').removeClass('hidden');
            } else {
                $('#createBookPopup').dialog('close');
            }
            var creatbook = $('#curSelPopup').html();
            $css.append('<ul id="' + lstC + '" class="nlist">' + creatbook + '</ul>');
            $bnp.val('');
        }); 
    } else {
        $('#clientSelector').addClass('closed');
        $('#myClients').addClass('closed');
        $('#byregion').removeClass('closed');
        $('#clientPopupSel').addClass('closed');
		$(document).on('click', '#saveCustomBook', function(e) {
            var bnp = $('#bookNamePopup').val(),
                bnpL = bnp.length,
                lstC = bnp.substr(0, 3),
                sdb = $('#setDfltBook').is(':checked'),
                chkdRdo = $('#bookNameRadio').find('input[type=radio]:checked').attr('rel'),
                chkd = chkdRdo.split(' ');
            if (sdb == true) {
                $(chkd[1]).append('<option class="txtblack" value="' + bnp + '" selected="selected" rel="' + lstC + '">' + bnp + '</option>');
                $(chkd[0]).append('<li rel="' + lstC + '">' + bnp + '</li>');
            } else {
                $(chkd[1]).append('<option class="txtblack" value="' + bnp + '" rel="' + lstC + '">' + bnp + '</option>');
                $(chkd[0]).append('<li rel="' + lstC + '">' + bnp + '</li>');
            }
            if ($('#bookNamePopup').val() == '') {
                e.preventDefault();
                $('#alertModule1').addClass('visible').removeClass('hidden');
            } else {
                $('#alertModule1').addClass('hidden').removeClass('visible');
                $('#createBookPopup').dialog('close');
            }
            $('#bookNamePopup').keyup(function() {
                if ($(this).val() != "") {
                    $('#alertModule1').addClass('hidden').removeClass('visible');
                }
            });
            var creatbook = $('#curSelPopup').html();
            $css.append('<ul id="' + lstC + '" class="nlist">' + creatbook + '</ul>');
            $('#bookNamePopup').val('');
        });  
    }

	(function() {
		var vendor_data = {
				series: [{
					type: 'pie',
					data: [
						['Fedilty', 34],
						['Vangard', 20],
						['Valic', 8],
						['Prudential', 26],
						['Fifth Vendor', 12]
					]
				}]
			},
			def_vendor_data = [
				['Fedilty', 34],
				['Vangard', 20],
				['Valic', 8],
				['Prudential', 26],
				['Fifth Vendor', 12],
				['Vendor6', 15],
				['Vendor7', 18],
				['Vendor8', 22],
				['Vendor9', 12],
				['Vendor10', 30]
			],
			databind = vendor_data,
			chartid = "renderPieChart_TransfersPie",
			chartP;

		function getCheckedIndex(elem) {
			var new_vendor_data = [];
			var _li = $('#vendorChatleg').find('ul').find('li').hide();

			$(elem).each(function(i) {
				if (this.checked) {
					if (new_vendor_data.length < 5) {
						new_vendor_data.push(def_vendor_data[i]);
					};
				}
			});

			new_vendor_data.map(function(obj, ind) {
				var ob = obj[0]
				_li.eq(ind).show().find('.legendFundName').html(ob)
			});
			return new_vendor_data;
		};

		chartP = new Highcharts.Chart({
			chart: {
				renderTo: chartid,
				height: 200,
				width: 300,
				padding: [0, 0, 0, 0],
				margin: [-20, 30, 30, 0],
				spacingTop: 0,
				spacingBottom: 0,
				plotBackgroundColor: null,
				plotBorderWidth: null,
				plotShadow: false
			},
			title: {
				text: ''
			},
			colors: ['#0052A3', '#7F3785', '#512D6D', '#00A0AF', '#658D1B'],
			legend: {
				layout: 'vertical',
				align: 'right',
				verticalAlign: '20',
				margin: [20, 0, 0, 0],
				borderWidth: 0
			},
			tooltip: {
				enabled: true,
				formatter: function() {
					return '$' + this.point.y + ',000,000';
				}
			},
			plotOptions: {
				pie: {
					allowPointSelect: false,
					cursor: 'pointer',
					dataLabels: {
						enabled: false
					},
					showInLegend: false
				}
			},
			credits: {
				enabled: false
			},
			series: databind.series
		});
		$(document).on('click', '.nTopven', function(e) {
			var _arr1 = getCheckedIndex($('.nTopven'));
			chartP.series[0].setData(_arr1);
		});
	})();
});
//$(document).on('headLoaded', function(){
//	(console) && console.log("headLoaded",arguments);
//});
//$(document).on('titleLoaded', function(){
//	(console) && console.log("titleLoaded",arguments);
//});
//$(document).on('leftLoaded', function(){
//	(console) && console.log("leftLoaded",arguments);
//});
//$(document).on('popupsLoaded', function(){
//	(console) && console.log("popupsLoaded",arguments);
//});