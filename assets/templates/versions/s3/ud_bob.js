$(document).on('bodyContLoaded', function () {
	$('.myTab').tabs();
	$(document).on('change','#bookselect', function(e){
		var bnpV = $(this).val(),
			bnp = bnpV.split('_');
		$('#curSelsSec').removeClass('closed')
		$('.selBook').html($(this).find(":selected").text());
		$('.curSels').html();
		$('#instCurSel').html(bnp[0]+' Institutions meet current selections:');
		$('#'+bnp[1]).removeClass('closed').siblings().addClass('closed');
		
	});
	
	if($('html').hasClass('ie7') == true || $('html').hasClass('ie6') == true){
		$("a[href*='#']").on('click', function(e){
		var iePopups = $(this).attr('href');
			iePopups = '#'+iePopups.substring(iePopups.lastIndexOf('#') +1);
			$(iePopups).dialog('open');
		});
	}
	
	$('#curSelsSec').on('click','.icdeleteIcon',function(e){
		$(this).closest('li').addClass('closed');
	});
	$(document).on('click','#clearAllRef',function(e){
		$('#curSelsSec').find('.nlist').addClass('closed');
		$('.curSels').addClass('closed');
	});
	$(document).on('click','#saveCustomBook',function(e){
		var bnp = $('#bookNamePopup').val(),
			bnpL = bnp.length,
			lstC = bnp.substr(0, 3),
			sdb = $('#setDfltBook').is(':checked');
			if(sdb == true){
				$('#bookselect').append('<option value="'+bnpL+'_'+lstC+'" selected="selected">'+bnp+'</option>');
				$('#mngCustLst').find('.nlist').append('<li rel="'+lstC+'">'+bnp+'</li>');
			}else{
				$('#bookselect').append('<option value="'+bnpL+'_'+lstC+'">'+bnp+'</option>');
				$('#mngCustLst').find('.nlist').append('<li rel="'+lstC+'">'+bnp+'</li>');
			}	
			$('#createBookPopup').dialog('close');
	});
	$(document).on('change','#actionSelect', function(e){
		if($(this).find(":selected").text() == "Rename"){
			$('#renameBook').removeClass('closed');
		}else{
			$('#renameBook').addClass('closed');
		}
	});
	$(document).on('click','#mngCustLst li',function(e){
		$(this).addClass('shdhlt').siblings().removeClass('shdhlt');
		$('#actionSelect').removeAttr('disabled');
	});
	$(document).on('click','#applyMngCust',function(e){
		var renBk = $('#renameBook').val(),
			actSel = $('#actionSelect').find(":selected").text();
		if(actSel == "Rename"){
			if(renBk != ''){
				$('#mngCustLst').find('.shdhlt').html('<span>'+renBk+'</span>');
				var renItem = $('#mngCustLst').find('.shdhlt').attr('rel');
				$('#bookselect').find('option[value*=_'+renItem+']').text(renBk);
				$('#renameBook').val('');
			}else{
				e.preventDefault();
			}
		}else if(actSel == "Set As Default Book"){
			var mcl = $('#mngCustLst').find('.shdhlt').text();
				$('#bookselect').append('<option selected="selected">'+mcl+'</option>');
				$('#manageBookPopup').dialog('close');
		}else if(actSel == "Delete"){
			var delItem = $('#mngCustLst').find('.shdhlt').attr('rel');
			$('#mngCustLst').find('.shdhlt').addClass('closed');
			$('#bookselect').find('option[value*=_'+delItem+']').remove();
			e.preventDefault();
		}else{
			e.preventDefault();
		}
	});
	
	$(document).on('click','#curSelPopup .icdeleteIcon',function(e){
		$(this).closest('li').addClass('closed');
	});
	
	$('#leftcol_bob .secondlevel').on('click',function(e){
		if($(this).attr('type') == 'checkbox'){
			if($(this).attr('checked')){
				$(this).closest('ul').parent().addClass('activeli')	;
			}
			else{
				$(this).closest('ul').parent().removeClass('activeli');	
			}
		}
		if($(this).attr('type') == 'radio'){
			if($(this).attr('checked')){
				$(this).closest('ul').parent().addClass('activeli').siblings().removeClass('activeli')	;
			}
		}
	});
	$('#leftcol_bob .thirdlevel').on('click',function(e){
		if($(this).attr('checked')){
			$(this).closest('ul').parent().addClass('activeli');
		}
		else{
			$(this).closest('ul').parent().removeClass('activeli');	
		}
		//console.log($(this).closest('.secondleveldiv').find('.shd2').html());
	});
	
	$(document).on('click','#channel, #lastVisited, #status', function(e){
		$('#ui-id-2').trigger('click');
		$('#tbl_ibob_interactions thead').find('th').eq(4).addClass('headerSortDown').siblings().removeClass('headerSortDown headerSortUp');
	});
	
	$(document).on('click','#opportunityType, #serviceModel, #salesPhase, #winProbability, #productAndServices', function(e){
		$('#ui-id-3').trigger('click');
	});
	
	

$('#leftcol_bob a.editLink').on('click',function(e){
	if($(this).attr('id') == 'winProb'){	
		$('#winProbability').attr('checked', false);
		var hoverPopup = $('#infodivWin');
			$(hoverPopup).addClass('visible').removeClass('closed');
			var boxW = $(hoverPopup).height();
			var infoHf = (boxW/2)+10; 
			var topPos = ($(this).offset().top)-infoHf;
			var leftPos = $(this).closest('li').width(); 
			$(hoverPopup).css({'top': topPos+'px','left': leftPos+40+'px'}).show();
	
	}else{
		$(this).closest('li').find('.secondleveldiv').removeClass('closed');
		$(this).find('.thirdpen').removeClass('closed');
		$(this).closest('.content').find('li.shd3').each(function( index ) {	
			$(this).removeClass('shd3');
			$(this).find('.clearspan').addClass('closed');
			$(this).find('.editLink').removeClass('closed');
			$(this).find('.secondleveldiv li').each(function(index) {		
				if($(this).hasClass('activeli') && $(this).parent().hasClass('nlist') ){		
					$(this).removeClass('closed');
					
				}else{
					if($(this).parent().hasClass('nlist')){
					$(this).addClass('closed');
			    }
			}
		});
		
		
		if($(this).find('.firstlevel').attr('checked')){		
			$(this).find('.firstlevel').removeClass('closed');	
		}else{		
			$(this).find('.firstlevel').removeClass('closed');
			$(this).find('.firstlevel').attr('checked','checked');
		}
	});
		$(this).closest('li').find('.clearspan').removeClass('closed');	
		$(this).closest('li').addClass('shd3');
		$(this).closest('li').find('.secondleveldiv').removeClass('closed');
		$(this).closest('li').find('.secondleveldiv').find('.secondlevel').removeClass('hidden');
		//$(this).closest('li').find('input[type=checkbox]:eq(0)').addClass('closed');	
		$(this).addClass('closed');
		$(this).closest('li').find('.secondleveldiv li').each(function( index ) {	
			$(this).parent().parent().removeClass('closed');
			if( $(this).parent().hasClass('nlist')){
			 $(this).find('input[type=checkbox]').removeClass('closed');
			 $(this).removeClass('closed');
			}
		});
	}		
	$('.thirdleveldiv').addClass('closed');
	$('.iceditLink').removeClass('closed');
	
});
	
$('#leftcol_bob .icclearLink').on('click',function(e){
	$(this).parent().parent().removeClass('shd3');
	$(this).parent().parent().find('.editLink').removeClass('closed');
	$(this).parent().parent().parent().find('.iceditLink').addClass('closed');
	$(this).parent().addClass('closed');
	if($(this).parent().parent().find('input[type=checkbox]:eq(0)').attr('checked')){	
		$(this).parent().parent().find('input[type=checkbox]:eq(0)').removeClass('closed');	
	}
	else{	
		$(this).parent().parent().find('input[type=checkbox]:eq(0)').removeClass('closed');
		$(this).parent().parent().parent().find('input[type=checkbox]:eq(0)').removeClass('closed');
		$(this).parent().parent().find('input[type=checkbox]:eq(0)').attr('checked','checked');
		$(this).parent().parent().parent().find('input[type=checkbox]:eq(0)').attr('checked','checked');
		var filterCount= $(this).closest('.content').prev().find('.filterval').html();
		filterCount=parseInt(filterCount);
		filterCount=filterCount+1;		
		$(this).closest('.content').prev().find('.filterval').html(filterCount);
	}
	$(this).parent().parent().find('.secondleveldiv').find('.secondlevel').addClass('hidden');
	//$(this).parent().parent().find('.thirdleveldiv').find('.thirdlevel').addClass('hidden');
	$(this).parent().parent().find('.secondleveldiv li').each(function(index) {		
			if($(this).hasClass('activeli')&&$(this).parent().hasClass('nlist') ){		
				$(this).removeClass('closed');
				$(this).find('.thirdleveldiv').removeClass('closed');
				
			}else{
				if($(this).parent().hasClass('nlist')){
				$(this).addClass('closed');
			}
		}
	});
	
	$(this).parent().parent().find('li').each(function( index ) {
	 if( $(this).parent().parent().hasClass('nlist')){	 
     $(this).find('input[type=checkbox]').addClass('closed');
	 $(this).addClass('closed');
	 }
	});
	
	var ct = 0;
	$(this).parent().parent().find('.secondleveldiv li').each(function() {	
		var chkbox = $(this).find('input[type=checkbox]').is(':checked'),
			radBtn = $(this).find('input[type=radio]').is(':checked');
		if(chkbox == true || radBtn == true){
			ct++;
		}
	});
	if(ct > 0){
		$(this).closest('li').find('.firstlevel').attr('checked',true);
	}else{
		$(this).closest('li').find('.firstlevel').attr('checked',false);
	}
	
	
	
	
});

//Third Level Functionality
	
	$('#leftcol_bob a.iceditLink').on('click',function(e){
		$(this).closest('li').find('.thirdleveldiv').removeClass('closed');
		$(this).closest('li').find('.thirdpen').removeClass('closed');	
		//$(this).find('.thirdpen').removeClass('closed');
		$(this).find('.iceditLink').addClass('closed');
		$(this).closest('li').find('.thirdleveldiv').find('.thirdlevel').removeClass('closed');
		$(this).addClass('closed');
		$(this).closest('.content').find('li.shd2').each(function( index ) {	
			$(this).removeClass('shd2');
			$(this).find('.thirdpen').addClass('closed');
			$(this).find('.iceditLink').removeClass('closed');
			$(this).find('.thirdleveldiv li').each(function(index) {		
				if($(this).hasClass('activeli') && $(this).parent().hasClass('nlist') ){		
					$(this).removeClass('closed');
				}else{
					if($(this).parent().hasClass('nlist')){
					$(this).addClass('closed');
			    }
			}
		});
		
		
		if($(this).find('.secondlevel').attr('checked')){		
			$(this).find('.secondlevel').removeClass('closed');	
		}else{		
			$(this).find('.secondlevel').removeClass('closed');
			$(this).find('.secondlevel').attr('checked','checked');
		}
	});
		$(this).closest('li').find('.thirdpen').removeClass('closed');	
		$(this).closest('li').addClass('shd2');
		$(this).closest('li').find('.thirdleveldiv').removeClass('closed');
		$(this).closest('li').find('.thirdleveldiv').find('.thirdlevel').removeClass('hidden');
		//$(this).closest('li').find('input[type=checkbox]:eq(0)').addClass('closed');	
		$(this).addClass('closed');
		$(this).closest('li').find('.thirdleveldiv li').each(function( index ) {	
			$(this).parent().parent().removeClass('closed');
			if( $(this).parent().hasClass('nlist')){
			 $(this).find('input[type=checkbox]').removeClass('closed');
			 $(this).removeClass('closed');
			}
		});
	});
	
	$('#leftcol_bob .thirdleveledit').on('click',function(e){
		$(this).parent().parent().removeClass('shd2');
		$(this).parent().parent().find('.iceditLink').removeClass('closed');
		$(this).parent().addClass('closed');
		if($(this).parent().parent().find('input[type=checkbox]:eq(0)').attr('checked')){	
			$(this).parent().parent().find('input[type=checkbox]:eq(0)').removeClass('closed');	
		}
		else{	
			$(this).parent().parent().find('input[type=checkbox]:eq(0)').removeClass('closed');
			$(this).parent().parent().find('input[type=checkbox]:eq(0)').attr('checked','checked');
			var filterCount= $(this).closest('.content').prev().find('.filterval').html();
			filterCount=parseInt(filterCount);
			filterCount=filterCount+1;		
			$(this).closest('.content').prev().find('.filterval').html(filterCount);
		}
		$(this).parent().parent().find('.thirdleveldiv').find('.thirdlevel').addClass('hidden');
		$(this).parent().parent().find('.thirdleveldiv li').each(function(index) {		
			if($(this).hasClass('activeli')&&$(this).parent().hasClass('nlist') ){		
				$(this).removeClass('closed');
				$(this).find('.thirdleveldiv').removeClass('closed');
				
			}else{
				if($(this).parent().hasClass('nlist')){
				$(this).addClass('closed');
			}
		}
	
	});
	
	$(this).parent().parent().find('li').each(function( index ) {
	 if( $(this).parent().parent().hasClass('nlist')){	 
     $(this).find('input[type=checkbox]').addClass('closed');
	 $(this).addClass('closed');
	 }
	});
	
	/*var ct = 0;
	$(this).parent().parent().find('.thirdleveldiv li').each(function() {	
		var chkbox = $(this).find('input[type=checkbox]').is(':checked'),
			radBtn = $(this).find('input[type=radio]').is(':checked');
		if(chkbox == true || radBtn == true){
			ct++;
		}
	});
	if(ct > 0){
		$(this).closest('li').find('.secondlevel').attr('checked',true);
	}else{
		$(this).closest('li').find('.secondlevel').attr('checked',false);
	}*/
});

	
	

//Third Level Functionality Ends Here


/*$('.secondlevel').on('click',function(e){
	$(this).closest('li').find('.thirdleveldiv').removeClass('closed');
});*/

$('#winCancel').on("click", function(e){
	$('#infodivWin').addClass('closed').removeClass('visible');
});

$( "#timeframe-slider" ).slider({
		orientation: "horizontal",
		range: true,
		value:100,
      min: 0,
      max: 100,
      step: 1,
		values: [ 10,30 ],
		slide: function( event, ui ) {
				$( "#range1" ).val( ui.values[ 0 ]);
				$( "#range2" ).val( ui.values[ 1 ]);
			}
		});
	//$('#range1').val($('#range1Val').text());
	//$('#range2').val($('#range2Val').text());
			
	

$(document).on('keyup','#range1, #range2', function(){
	var $ran1 = $(this).val(),
	num = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/,
	slider = $(this).closest('.infoHover').find('.slider');
	if(!num.test($ran1) || $ran1 > 100) {	
		$(this).addClass('descl');
	}else{
		$(this).removeClass('descl');
		if($(this).attr('id') == 'range1'){
			slider.find('a').eq(0).css({'left': $ran1+'%'});
		}else{
			slider.find('a').eq(1).css({'left': $ran1+'%'});
		}
	}
	var r1 = $('#range1').val(),
		r2 = $('#range2').val();
	if(r1 > r2){
		var rng = r1-r2;
		$(this).closest('.infoHover').find('.ui-slider-range').css({'left': r2+'%','width':rng+'%'});
	}else{
		var rng = r2-r1;
		$(this).closest('.infoHover').find('.ui-slider-range').css({'left': r1+'%','width':rng+'%'});
	}	
	
	
});			
			
$('#applySlider').on("click", function(e){
	$('#range1Val').html($("#range1").val());
	$('#range2Val').html($("#range2").val());
	$('#winProbability').attr('checked',true);
	$('.winProbtxt').removeClass('closed');
	$('#infodivWin').addClass('closed').removeClass('visible');
});

$('#leftcol_bob .firstleveldiv').on('click',function(e){
	if($(this).attr('id') == 'winProbability'){
		e.preventDefault();
	}else{
		$(this).closest('li').find('a.editLink').trigger('click');
	}
});

/*$('#leftcol_bob .secondlevel').on('click',function(e){
	$(this).closest('li').find('a.iceditLink').trigger('click');
});*/

$('#contentColumn').on('scroll', function() {
	$('#infodivWin').addClass('closed').removeClass('visible');
});


$('.selview').on('click',function() {
 $('.tbldata').addClass('closed');
  $('li ul, li div.scrollContainer').addClass('closed');  
  $('div.first h3 a').html('<span class="icon"></span>' + $(this).parent('li').find('span.txtlc').text() +' Summary');
   if($(this).attr('id') =='Bob' || $(this).attr('id') =='Market' || $(this).attr('id') =='allClients') {
	$('#ibob').removeClass('closed');
  }
  else if($(this).attr('id') =='Region') {
	$('#regions').removeClass('closed');
  }
  else {
	$('#stat').removeClass('closed');
  }
	var subID = 'sub'+$(this).attr('id');
	if ($('#'+subID).length){
	   $('#'+subID+',#'+subID+' ul').removeClass('closed');
	}
	
	
});
$('.narview').on('click',function() {
	$('.subnarrow').addClass('closed');								  
	var subnarview =  'sub' + $(this).attr('id');
	$('.' +subnarview).removeClass('closed');
});

$('.toggleMore').on('click', function(){
	$(this).next().toggle();
});
			
});